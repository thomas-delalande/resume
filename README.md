# Tom Delalande
`Software Engineer`

Hi I'm Tom. I'm a Software Engineer currently employed at VGW. I really love solving problems and writing good code. I'm comfortable adapting with absolutely any language/environment but have the most experience with Kotlin and Typescript. I'm a sucker for multi-paradigm languages that allow me to use functional concepts as much as possible.
### Experience & Education
**Virtual Gaming Worlds - Associate Engineer**
- _April 2021 - Present_
- VGW has a state of the art learning and development program. One which has greatly increased my abilities as a developer, through which I've practically learnt about Test Driven Development, Web APIs and Eventsourced systems. I have experience working as part of multiple teams on both front-end and back-end features.

**Curtin University - Computer Science**

- _Completed June 2021_


**AWS Solutions Architect Associate Certificate**

- _Completed July 2021_

### Personal Projects  _(Breakable Toys)_
**Startup Weekend Social Network**
- I participated in and won Startup Weekend Perth 2021. Where I built an application that allowed users to create event and invite people using a link. The application was built using React Framework for the frontend, Firebase Firestore for the backend and Netlify for CI/CD. 

**Flutter Mobile App**
- I have published Android app that I built from scratch using the Flutter framework. This is a simple application that create randomised workouts according to user specification and animates a timer for the created workout.
_com.delalande.jim_

**AI Search Visualisation Tool**
- As part of the Capstone Computing Project Unit I took I worked on a team to build a tool that can be used by students taking Artificial Intelligence Units to learn about search algorithms and how they function.

**Multiplayer Browser Game**
- I have built a small online strategy multiplayer game that uses server sent events to communicate to players in real time. It uses Svelte for the front end, Kotlin for the backend and is deployed to an EC2.

**Todo Lists**
- I have made many Todo lists in countless frameworks/languages. Including Svelte, React, Elm. Although I don't know if I would use Elm in production, I love the code than functional languages produce.
